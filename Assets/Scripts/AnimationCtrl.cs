﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimState { Idle, Move, Attack, Dead }

[RequireComponent(typeof(Animator))]
public class AnimationCtrl : MonoBehaviour {

	Animator animator;
	Animator Animator {
		get {
			if (animator == null)
				animator = GetComponent<Animator>();
			return animator; 
		}
	}
	public AnimState state;

	// Use this for initialization
	void Start () {

	}
	
	public void SetAttackSpeed (float speed) {
		Animator.SetFloat("Attack_Speed", speed);
	}

	// Update is called once per frame
	public void SetState (AnimState state) {
		this.state = state;
		Animator.SetBool("Move", this.state == AnimState.Move);
		Animator.SetBool("Attack", this.state == AnimState.Attack);
	}
}
