﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStageChangeCtrl : MonoBehaviour
{
    private int index = 0;
    private int stageCount;
    public RectTransform stageRT;
    public UIStageData stageTMP;
    public bool loopShow = true;

	// Use this for initialization
	void Start () {
        stageCount = DataBaseManager.ctrl.stageDB.stageDatas.Count;
        stageRT.sizeDelta = new Vector2(stageCount * 1500 , 0);
        for (int i = 0; i < stageCount; i++)
        {
            Instantiate(stageTMP, stageRT).SetData(DataBaseManager.ctrl.stageDB.stageDatas[i]);
        }
	}
	
	public void Left()
    {
        index--;
        if (index < 0) index = loopShow ? stageCount - 1 : 0;
        stageRT.anchoredPosition = new Vector2(-1500 * index ,0);
    }

    public void Right()
    {
        index++;
        if (index >= stageCount) index = loopShow ? 0 : stageCount - 1;
        stageRT.anchoredPosition = new Vector2(-1500 * index, 0);
    }
}
