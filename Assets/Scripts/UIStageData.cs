﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIStageData : MonoBehaviour
{
    public Image iconImg;
    public Text descText;
    public Text enterTipText;

    public StageData stageData;

    /// <summary>
    /// 設定UI內StageData
    /// </summary>
    /// <param name="stageData">從資料庫匯入的關卡資料</param>
	public void SetData(StageData stageData)
    {
        gameObject.SetActive(true);
        this.stageData.name = stageData.name;
        this.stageData.stageID = stageData.stageID;
        this.stageData.description = stageData.description;
        this.stageData.powerCost = stageData.powerCost;
        this.stageData.icon = stageData.icon;

        descText.text = this.stageData.name;
        enterTipText.text = "消耗能量"+ this.stageData.powerCost + "點";
        iconImg.sprite = this.stageData.icon;
    }

    public void EnterStage()
    {
        if (!GameManager.ctrl.infoSystem.SetPower(-stageData.powerCost)) return;
        SceneManager.LoadScene(stageData.stageID);
    }
}
