﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 系統介面，於遊戲內物件繼承使用
/// </summary>
public class ISystem : MonoBehaviour
{
    public GameManager m_GM; //資料實體
    public GameManager GM //外部接口
    {
        get
        {   //避免記憶體額外的配置
            if(m_GM == null) m_GM = new GameManager();
            return m_GM;
        }
    }
	
}
