﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

public class GameManager
{
    public static GameManager ctrl;//靜態欄位，表示唯一的自己

    private PlayerInfoSystem m_infoSystem;
    public PlayerInfoSystem infoSystem
    {
        get { return m_infoSystem; }
    }

    private TargetSystem m_targetSystem;
    public TargetSystem targetSystem {
        get {
            //第一次啟動目標系統
            if (m_targetSystem == null) m_targetSystem = new TargetSystem();
            return m_targetSystem;
        }
    }

    /// <summary>
    /// 建構GM上線
    /// </summary>
    public GameManager()
    {
        if (ctrl != null) return;
        ctrl = this;
        Debug.Log("GM is online .");
        //GM開始逐步啟動系統
        m_infoSystem = new PlayerInfoSystem();
    }
}

public class TargetSystem {
    public List<MonsterCtrl> monsterList { get; private set; }

    public TargetSystem() {
        monsterList = new List<MonsterCtrl>();
    }

    public void Add(MonsterCtrl monster) {
        monsterList.Add(monster);
    }

    public MonsterCtrl SearchNearest (Transform fromPos) {
        MonsterCtrl monster = null;
        float range = Mathf.Infinity;
        for (int i = 0; i < monsterList.Count; i++) {
            if (Vector3.Distance(fromPos.position, monsterList[i].transform.position) < range) {
                range = Vector3.Distance(fromPos.position, monsterList[i].transform.position);
                monster = monsterList[i];
            } 
        }

        return monster;
    }
}

public enum TimerType { CountUp , CountDown }
/// <summary>
/// 計時器(使用密封禁止繼承)
/// </summary>
public sealed class Timer
{
    public TimerType type { get; private set; }
    public float duration { get; private set; }
    public float time { get; private set; }
    public bool isDone
    {
        get { return Update(Time.deltaTime); }
    }

    /// <summary>
    /// 創建計時器(可設定正數/倒數)
    /// </summary>
    /// <param name="duration">計時長度</param>
    /// <param name="type">時間"正數/倒數"模式</param>
    public Timer(float duration, TimerType type = TimerType.CountUp)
    {
        this.duration = duration;
        this.type = type;
        Start();
    }
    /// <summary>
    /// 初始化計時器
    /// </summary>
    public void Start()
    {
        switch (type)
        {
            case TimerType.CountUp:
                time = 0f;
                break;

            case TimerType.CountDown:
                time = duration;
                break;
        }
    }

    /// <summary>
    /// 更新計時器時間
    /// </summary>
    /// <param name="t">更新間隔</param>
    public bool Update(float t)
    {
        switch (type) {
            case TimerType.CountUp:
                time += t;
                return time >= duration;

            case TimerType.CountDown:
                time -= t;
                return time <= 0;

            default:
                return true;
        }
    }
}
