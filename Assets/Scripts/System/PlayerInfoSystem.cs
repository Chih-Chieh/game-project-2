﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfoSystem
{
    #region 人物基本資訊
    public string playerName { get; private set; }
    public int level { get; private set; }
    public float exp { get; private set; }
    public const float expBase = 100f;//經驗值基礎數據
    public float currentExp //當前經驗值：總經驗 - 前一級之前經驗總合(分子)
    {
        get { return exp - BeforeExp(); }
    }
    public float expMax //當級需求經驗值(分母)
    {
        get { return expBase * level; }
    }
    public float expPercent
    {
        get { return currentExp / expMax; }
    }
    #endregion

    #region 貨幣資訊
    public int money { get; private set; }
    public int diamond { get; private set; }
    public int power { get; private set; }
    public const int powerMax = 100;
    public string powerStr
    {
        get { return power.ToString() + "/" + powerMax.ToString(); }
    }
    public bool runRecovery { get { return power < powerMax; } }
    #endregion

    public PlayerInfoSystem()
    {
        playerName = "無名氏";
        level = 1;
        exp = 0;
        money = 100;
        diamond = 0;
        power = 0;
    }

    #region 經驗值計算
    /// <summary>
    /// 設定經驗值(增加或減少)
    /// </summary>
    /// <param name="exp">變動的經驗值</param>
    public void SetExp(float exp)
    {
        this.exp += exp;
        while (currentExp >= expMax)
        {
            level++;
        }
    }

    /// <summary>
    /// 經驗值加總
    /// </summary>
    /// <returns>前一級之前的所有經驗</returns>
    public float BeforeExp()
    {
        float total = 0;
        for (int i = 1; i < level; i++)
        {
            total += expBase * i;
        }
        return total;
    }
    #endregion

    #region 貨幣計算相關
    /// <summary>
    /// 更新Money數值
    /// </summary>
    /// <param name="money">使用Money數值</param>
    public void SetMoney(int money)
    {
        if (this.money + money < 0) return;
        this.money += money;
    }

    /// <summary>
    /// 更新Diamond數值
    /// </summary>
    /// <param name="diamond">使用Diamond數值</param>
    public void SetDiamond(int diamond)
    {
        if (this.diamond + diamond < 0) return;
        this.diamond += diamond;
    }

    /// <summary>
    /// 能量恢復功能
    /// </summary>
    public void RecoveryPower()
    {
        if (power >= powerMax) return;
        power++;
    }

    /// <summary>
    /// 更新Power數值
    /// </summary>
    /// <param name="power">使用Power數值</param>
    /// <returns>是否成功扣除</returns>
    public bool SetPower(int power)
    {
        if (this.power + power < 0) return false;

        this.power += power;
        if (this.power >= powerMax)
        {
            this.power = powerMax;
        }
        return true;
    }

    #endregion
}
