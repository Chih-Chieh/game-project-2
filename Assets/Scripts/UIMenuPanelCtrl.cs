﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;//使用介面的程式庫

public class UIMenuPanelCtrl : MonoBehaviour {
	public UICanvasGroupCtrl[] objs;

	public void ToggleSwitch(Toggle toggle)
	{
		//print(toggle.name + ":" + toggle.isOn);
		switch (toggle.name)
		{
			case "Toggle (角色)":
				objs[0].Switch(toggle.isOn);
				break;
			case "Toggle (背包)":
				objs[1].Switch(toggle.isOn);
				break;
			case "Toggle (冒險)":
				objs[2].Switch(toggle.isOn);
				break;
			case "Toggle (設定)":
				objs[3].Switch(toggle.isOn);
				break;
		}
	}
}
