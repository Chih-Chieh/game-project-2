﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class UICanvasGroupCtrl : MonoBehaviour
{
    private CanvasGroup m_CG;//CanvasGroup實體對象
    public CanvasGroup CG//資料接口
    {
        get
        {
            if (m_CG == null) m_CG = GetComponent<CanvasGroup>();//第一次使用時
            return m_CG;
        }
    }

	// Use this for initialization
	void Start () {
        //CG = GetComponent<CanvasGroup>();
	}
	
	public void Switch(bool B)
    {
        CG.blocksRaycasts = B;
        CG.alpha = B ? 1 : 0;
    }

    public void Switch()
    {
        CG.blocksRaycasts = !CG.blocksRaycasts;
        CG.alpha = CG.blocksRaycasts ? 1 : 0;
    }
}
