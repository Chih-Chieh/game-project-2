﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSystem : MonoBehaviour {

	public static CameraSystem ctrl;
	public Transform cameraTarget {
		get; private set;
	}
	public Transform target {
		get; private set;
	}
	[Range(0, 10)]
	public float followSpeed = 5f;
	Vector3 targetPos;
	public bool autoUpdate;

	[Header("Distance settings")]
	public float distance = 5f;
	[Header("Angle settings")]
	[Range(15, 60)]
	public float angX = 15f;
	[Range(0, 360)]
	public float angY = 0f;

	void Awake() {
		ctrl = this;
	}

	// Use this for initialization
	void Start () {
		// Creates a new object for camera to follow 
		cameraTarget = new GameObject("CameraTarget").transform;
		transform.SetParent(cameraTarget);
		// Calculates the camera position relative to the target position
		transform.position = FollowTarget();
		transform.LookAt(cameraTarget); // Makes the camera point at the target
	}
	
	// Update is called once per frame
	void Update () {
		if (autoUpdate) {
			transform.position = FollowTarget();
			transform.LookAt(cameraTarget);
			if (target)
				CameraUpdate();
		}
	}

	// This function is called by others to make the camera follow
	// Due to it relies on another class' variable (less depedency)
	public void CameraUpdate(Vector3 pos) {
		targetPos = pos;
		// The newly created object will follow the player position instead. 
		// This is to eliminate "jitteryness" from camera following the player directly
		cameraTarget.position = Vector3.Lerp(cameraTarget.position, targetPos, Time.deltaTime * followSpeed);
	}

	// This function will be called automatically if target transform actually contains something 
	public void CameraUpdate() {
		cameraTarget.position = Vector3.Lerp(cameraTarget.position, target.position, Time.deltaTime * followSpeed);
	}

	// To set camera position: angle x distance from target
	Vector3 FollowTarget() {
		return cameraTarget.position + Angle() * Distance();
	}

	Vector3 Distance() {
		return Vector3.back * distance;
	}

	Quaternion Angle() {
		return Quaternion.Euler(angX, angY, 0);
	}
}
