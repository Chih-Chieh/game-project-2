﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FaceDir { Up, Right, Left, Down }

/// <summary>
/// 發射點資訊
/// </summary>
public struct ShootPointData {
	public Vector3 pos { get; private set; }
	public float ang { get; private set; }
	public Quaternion quat { get; private set; }
	public int arcCount { get; private set; }//弓型的子彈數(分母)

	public float offset;
	public bool isEven;//whether its even amount of bullets
	public bool isCircle;//whether to fire in a circle-shaped (loop)

	public void SetVal(Vector3 pos, float ang, int bowCount = 0, bool isCircle = false) {
		this.pos = pos;
		this.ang = ang;
		this.arcCount = bowCount;
		this.isCircle = isCircle;
	}

	public Vector3 ShootPoint() {
		quat = Quaternion.AngleAxis(ang, Vector3.up);
		// Forward direction vector
		Vector3 forwardVT = quat * (Vector3.forward * 1.3f);
		// Obtains perpendicular direction vector (left and right in this case)
		// Cross product of up and forward vectors
		Vector3 rightVT = Vector3.Cross(forwardVT, Vector3.up) *
			(offset * 0.2f - (isEven ? Mathf.Sign(offset) * 0.1f : 0));
		//pos += Vector3.right * offset;
		return pos + forwardVT + rightVT;
	}

	/// <summary>
	/// Gets the shooting point on the arc
	/// </summary>
	/// <param name="ang">arc range</param>
	/// <returns>Shooting point</returns>
	public Vector3 ShootPoint(float includedAng = 360) {
		float cellAng = includedAng / (arcCount - 1);
		quat = Quaternion.AngleAxis(cellAng * offset + ang, Vector3.up);
		Vector3 forwardVT = quat * (Vector3.forward * 1.3f);

		return pos + forwardVT;
	}

	public Vector3 CircleShootPoint() {
		float cellAng = 360 / arcCount;//切披薩
		quat = Quaternion.AngleAxis(cellAng * offset + ang, Vector3.up);
		//前方向量
		Vector3 forwardVT = quat * (Vector3.forward * 1.3f);
		return pos + forwardVT;
	}
}

public struct ShootPointType {
	public int frontPoint;
	public int sidePoint;
	public int backPoint;
	public int arcPoint;
	public int circlePoint;

	public bool side { get { return sidePoint > 0; } }
	public bool back { get { return backPoint > 0; } }
	public bool circle { get { return circlePoint > 0; } }
}

public static class TransformExtension {
	/// <summary>
	/// 發射點清單
	/// </summary>
	/// <param name="pos">發射原點</param>
	/// <param name="ang">面向角</param>
	/// <param name="spt">各方位點數量</param>
	/// <returns>發射點清單</returns>
	public static List<ShootPointData> ShootPoint(this Vector3 pos, float ang, ShootPointType spt) {
		List<ShootPointData> pointList = new List<ShootPointData>();
		ShootPointData SPD = new ShootPointData();

		if (spt.circle) {
			//圓型發射點
			foreach (ShootPointData tmpSPD in CircleShootPoint(pos, ang, spt.circlePoint)) {
				pointList.Add(tmpSPD);
			}
		}

		//彈弓型發射點(對稱兩顆)
		for (int i = 2; i <= spt.arcPoint * 2 + 1; i++) {
			SPD.offset = ((i % 2) > 0 ? 1 : -1) * (i / 2);
			SPD.SetVal(pos, ang, spt.arcPoint * 2 + 1);
			pointList.Add(SPD);
		}

		// frontPoint will fire ONCE regardless hence "<=" & .frontPoint  + 1 to compensate for the initial bullet
		for (int i = 1; i <= spt.frontPoint + 1; i++) {
			SPD.isEven = (spt.frontPoint + 1) % 2 == 0;
			int I = i + (SPD.isEven ? 1 : 0);
			SPD.offset = ((I % 2) > 0 ? 1 : -1) * I / 2;
			SPD.SetVal(pos, ang);
			//SPD.pos = pos;
			//SPD.quat = Quaternion.AngleAxis(ang, Vector3.up); // converts to attackAng to Quarternion to use in actual rotation
			pointList.Add(SPD);
		}

		if (spt.back) {
			// i starts at 1 to prevent 2nd point overlapping 1st
			for (int i = 1; i <= spt.backPoint; i++) {
				// whether bullet is even or odd amount
				SPD.isEven = spt.backPoint % 2 == 0;
				// if even amount, first bullet wont be centered.
				// A.k.a. i starts at 2 = offset will start at -1 then 1 instead of 0
				int I = i + (SPD.isEven ? 1 : 0);
				// i % 2 will decide whether its left or right. i / 2 will make sure the offset is same distance for next two shots
				SPD.offset = ((I % 2) > 0 ? 1 : -1) * I / 2;
				SPD.SetVal(pos, ang + 180);
				pointList.Add(SPD);
			}
		}

		if (spt.side) {
			//兩側的發射點
			for (int i = 1; i <= spt.sidePoint; i++) {
				SPD.isEven = spt.sidePoint % 2 == 0;
				int I = i + (SPD.isEven ? 1 : 0);
				SPD.offset = ((I % 2) > 0 ? 1 : -1) * I / 2;
				SPD.SetVal(pos, ang + 90);
				pointList.Add(SPD);
				SPD.SetVal(pos, ang - 90);
				pointList.Add(SPD);
			}
		}

		return pointList;
	}

	public static List<ShootPointData> CircleShootPoint(this Vector3 pos, float ang, int count) {
		List<ShootPointData> pointList = new List<ShootPointData>();
		ShootPointData SPD = new ShootPointData();
		for (int i = 0; i < count; i++) {
			SPD.offset = i;
			SPD.SetVal(pos, ang, count, true);
			pointList.Add(SPD);
		}
		return pointList;
	}

	#region 測試
	public static Vector3 ShootPoint(this Transform origTransform, FaceDir dir, float dist = 1f) {

		Vector3 point = Vector3.zero;

		switch (dir) {
			case FaceDir.Up:
				point = origTransform.forward * dist;
				break;

			case FaceDir.Right:
				point = origTransform.right * dist;
				break;

			case FaceDir.Left:
				point = -origTransform.right * dist;
				break;

			case FaceDir.Down:
				point = -origTransform.forward * dist;
				break;
		}

		return origTransform.position + point;
	}

	public static Quaternion ShootRotate(this Transform origTransform, FaceDir dir, float ang = 0) {
		Quaternion quaternion = Quaternion.identity;
		float angle = 0;

		switch (dir) {
			case FaceDir.Up:
				angle = ang;
				break;
			case FaceDir.Right:
				angle = ang + 90;
				break;
			case FaceDir.Left:
				angle = ang - 90;
				break;
			case FaceDir.Down:
				angle = ang + 180;
				break;
		}
		quaternion = Quaternion.AngleAxis(angle, Vector3.up);
		return quaternion;
	}
	#endregion
}
