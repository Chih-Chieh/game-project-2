﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtrl : MonoBehaviour 
{
	public static PlayerCtrl ctrl;
	public CharacterController charCtrl;

	Vector2 faceVector;
	public Vector2 FaceVector {
		get {
			faceVector.x = Input.GetAxisRaw("Horizontal");
			faceVector.y = Input.GetAxisRaw("Vertical");
			return faceVector;
		}
	}

	bool isMoving {
		get { return FaceVector != Vector2.zero; }
	}

	[Header("Attack Speed Setting")]
	[Range(0.5f, 2f)]
	public float attackSpeed = 1f;

	Timer timer;
	public Timer Timer {
		get {
			if (timer == null)
				timer = new Timer(1f / attackSpeed); // (1f / attackSpeed) to obtain proper attack speed multiplier. 
													 // 1f denotes the speed of the animation clip

			return timer;
		}
	}

	[Header("Ammo prefab")]
	public AmmoCtrl ammo;
	public ShootPointType SPT;
	[Header("Target prefab")]
	public Transform target;

	float mapW = 50;
	float mapH = 25;

	Vector3 focusPoint;
	public Vector3 FocusPoint {
		get {
			focusPoint = transform.position;
			focusPoint.x = Mathf.Clamp(focusPoint.x, -(mapW / 2 - 5), mapW / 2 - 5);
			focusPoint.z = Mathf.Clamp(focusPoint.z, -(mapH / 2 - 7), mapH / 2 - 7);
			return focusPoint; 
		}
	}

	AnimationCtrl animationCtrl;
	AnimationCtrl AnimationCtrl {
		get {
			if (animationCtrl == null)
				animationCtrl = GetComponentInChildren<AnimationCtrl>();
			return animationCtrl;
		}
	}
	Vector3 attackPos;
	float attackAng;

	void Awake() {
		ctrl = this;
	}

	// Use this for initialization
	void Start () {
		AnimationCtrl.SetAttackSpeed(attackSpeed);
		SPT.arcPoint = 0;
		SPT.frontPoint = 0; // This is the multiplier value rather than the actual amount
		SPT.backPoint = 0; // Actual bullets amount fired value
		SPT.sidePoint = 0; // Actual bullets amount fired value
		SPT.circlePoint = 9;
	}
	
	// Update is called once per frame
	void Update () {
		if (isMoving) {
			AnimationCtrl.SetState(AnimState.Move);
			Move();
			Rota();
		} else if (GameManager.ctrl.targetSystem.SearchNearest(transform)) {
			target = GameManager.ctrl.targetSystem.SearchNearest(transform).transform;
			transform.LookAt(target);
			if (Timer.isDone) {
				AnimationCtrl.SetState(AnimState.Attack);
				attackPos = transform.position;
				// Retrieves the amount of angle to turn to, based on the world forward pos
				attackAng = Vector3.Angle(Vector3.forward, transform.forward) * Mathf.Sign(transform.forward.x);
				Invoke("Attack", 0.5f * (1f / attackSpeed));
				Timer.Start();
			} 
		}

		CameraSystem.ctrl.CameraUpdate(FocusPoint);
	}

	void Attack() {
		List<ShootPointData> pointList = attackPos.ShootPoint(attackAng, SPT);
		foreach(ShootPointData data in pointList) {
			if (data.arcCount > 0 && !data.isCircle) {
				Instantiate(ammo, data.ShootPoint(70), data.quat);
			} else if (data.isCircle) {
				Instantiate(ammo, data.CircleShootPoint(), data.quat);
			} else 
				Instantiate(ammo, data.ShootPoint(), data.quat);
		}
		

		//Instantiate(ammo, transform.ShootPoint(FaceDir.Up, 0.8f), transform.ShootRotate(FaceDir.Up, attackAng));
		//Instantiate(ammo, transform.ShootPoint(FaceDir.Right, 0.8f), transform.ShootRotate(FaceDir.Right, attackAng));
		//Instantiate(ammo, transform.ShootPoint(FaceDir.Down, 0.8f), transform.ShootRotate(FaceDir.Down, attackAng));
		//Instantiate(ammo, transform.ShootPoint(FaceDir.Left, 0.8f), transform.ShootRotate(FaceDir.Left, attackAng));
	}

	void Move() {
		//Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
		//Vector3 inputDir = input.normalized;
		charCtrl.SimpleMove(transform.forward * Time.deltaTime * 300);
		
	}

	void Rota() {
		float ang = Vector2.Angle(Vector2.up, faceVector) * Mathf.Sign(faceVector.x);
		transform.rotation = Quaternion.AngleAxis(ang, Vector3.up);
	}
}
