﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerInfoCtrl : ISystem
{
    #region 人物基本資訊
    [Header("人物基本資訊")]
    public Image headImg;
    public Image expBarImg;
    public Text levelText;
    public Text playerNameText;
    #endregion

    #region 貨幣資訊
    [Header("貨幣資訊")]
    public Text moneyText;
    public Text diamondText;
    public Text powerText;
    #endregion

    private Timer m_timer;
    private Timer timer
    {
        get
        {
            if (m_timer == null) m_timer = new Timer(5);
            return m_timer;
        }
    }

    // Use this for initialization
    void Start () {
        UpdatePlayerName();
        UpdateLevelUI();
        GM.infoSystem.SetExp(3300f);//經驗值增加
        GM.infoSystem.SetPower(10);//POWER值增加
        UpdateExpUI();

        UpdateMoneyUI();
        UpdateDiamondUI();
        UpdatePowerUI();
    }

	/// <summary>
    /// 回復體力同時更新顯示UI
    /// </summary>
    void RecoveryPower()
    {
        GM.infoSystem.RecoveryPower();
        UpdatePowerUI();
    }
    // Update is called once per frame
    void Update () {
        if (GM.infoSystem.runRecovery && timer.isDone)
        {
            RecoveryPower();
            timer.Start();
        }else timer.Update(Time.deltaTime);

        /*if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            SetPower(30);
            SetMoney(1000);
            SetDiamond(100);
        }

        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            SetPower(-50);
            SetMoney(-2500);
            SetDiamond(-250);
        }*/
    }

    #region UI更新
    /// <summary>
    /// 更新人物名稱
    /// </summary>
    public void UpdatePlayerName()
    {
        playerNameText.text = GM.infoSystem.playerName;
    }
    /// <summary>
    /// 更新等級數據顯示
    /// </summary>
    public void UpdateLevelUI()
    {
        levelText.text = GM.infoSystem.level.ToString();
    }
    /// <summary>
    /// 更新經驗值數據顯示(附加等級更新)
    /// </summary>
    public void UpdateExpUI()
    {
        UpdateLevelUI();
        expBarImg.fillAmount = GM.infoSystem.expPercent;
    }
    /// <summary>
    /// 更新金幣數據顯示
    /// </summary>
    public void UpdateMoneyUI()
    {
        moneyText.text = GM.infoSystem.money.ToString();   
    }
    /// <summary>
    /// 更新鑽石數據顯示
    /// </summary>
    public void UpdateDiamondUI()
    {
        diamondText.text = GM.infoSystem.diamond.ToString();
    }
    /// <summary>
    /// 更新能量數據顯示
    /// </summary>
    public void UpdatePowerUI()
    {
        powerText.text = GM.infoSystem.powerStr;
    }
    #endregion


}
