﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//場景管理程式庫

public class SceneChanger : MonoBehaviour 
{
    public void SceneChangeByName(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
