﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 關卡資料結構
/// </summary>
[System.Serializable]
public struct StageData
{
    public string name;//關卡標題
    public string stageID;//關卡ID
    public string description;//關卡說明
    public int powerCost;//關卡進入門檻(耗能)
    [Header("關卡圖示")]
    public Sprite icon;//關卡圖示
}

[CreateAssetMenu(fileName = "StageDB", menuName = "DB/StageDB", order = 0)]
public class StageDB : ScriptableObject
{
    public List<StageData> stageDatas;
}
